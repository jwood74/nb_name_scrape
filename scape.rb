#!/usr/bin/ruby

require 'nokogiri'

site = 'http://' #This needs to be a page on a NationBuilder site with a list of people signups down the bottom
pages = 5 #Sometimes the page can be 1 of 5. Select the correct number of pages for the scrape to cycle through

filed = ''
$cnt = 1
while $cnt <= pages
	`wget -nv '#{site}?page=#{$cnt}' --output-document=names_#{$cnt}.xml`
	$cnt +=1
end

$cnt = 1
while $cnt <= pages
	xmlfile = 'names_' + $cnt.to_s + '.xml'
	doc = Nokogiri::XML(File.open(xmlfile))
	li = doc.xpath(".//li[@class='activity clearfix']")

	li.each do |ll|
		nm = ll.xpath(".//span[@class='linked-signup-name']")
		if nm.nil?
			next
		end
		cl = ll.xpath(".//span[@class='tag']")
		filed << nm.text.to_s + '@' + cl.text.to_s + "\n"
	end
	puts 'page ' + $cnt.to_s
	$cnt +=1
end

puts 'writing file at names.txt'
File.open("names.txt", 'w') { |file| file.write(filed) }

puts 'deleting old files'
$cnt = 1
while $cnt <= pages
	`rm names_#{$cnt}.xml`
	$cnt +=1
end