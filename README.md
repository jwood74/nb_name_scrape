# README #

Do you ever see a website asking people to volunteer, and it lists all the previous people who have volunteered?
And you think to yourself two things-
1- Why did the website designer leave this as public
2- How hard would it be to get a list of everyone who said they wanted to volunteer

### What is this repository for? ###

* Simply ruby script to scrape names off a NationBuilder site of people who have volunteered.
* This would presumably also work on people who have donated (if this is also shown publically)
* Not limited to NationBuilder - but would require some jigging around to get other html tags to work

### How do I get set up? ###

* Requires Ruby
* Requires Nokogiri ruby gem
* Once the dependancies are installed, and the repository is set up open scrape.rb
* There are two variables to edit-
* * Site: the URL of the page
* * Pages: the number of pages of names

### Who do I talk to? ###

* Jaxen Wood - jwood74
* jw@jaxenwood.com